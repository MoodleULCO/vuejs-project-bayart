export default class Data {
	static getData() {
		const listOfList = localStorage.getItem('list');

		if(!listOfList) {
			localStorage.setItem('list', '{}');
			return {};
		}

		return JSON.parse(listOfList);
	}

	static createList(name) {
		const id = name.replace(/\W/g, '').toLowerCase();

		this.updateList(
			id,
			{
				name,
				items: [],
			}
		);

		return id;
	}

	static updateList(id, listData) {
		const data = this.getData();

		data[id] = listData;

		localStorage.setItem('list', JSON.stringify(data));
	}

	static addItemToList(id, name) {
		const data = this.getData();

		data[id].items.push({
			checked: false,
			name,
		});

		localStorage.setItem('list', JSON.stringify(data));
	}

	static checkItemFromList(id, name) {
		const data = this.getData();

		data[id].items = data[id].items.map((elt) => {
			if(elt.name === name) {
				elt.checked = !elt.checked;
			}

			return elt;
		});

		localStorage.setItem('list', JSON.stringify(data));
	}

	static deleteItemFromList(id, name) {
		const data = this.getData();

		data[id].items = data[id].items.filter((elt) => elt.name !== name);

		localStorage.setItem('list', JSON.stringify(data));
	}

	
	static getLists() {
		const listOfList = this.getData();

		const finalData = [];
		for(const id in listOfList) {
			finalData.push({
				id,
				name: listOfList[id].name,
				itemCount: listOfList[id].items.length
			});
		}

		return finalData;
	}

	static getContent(id) {
		return { 
			...(this.getData()[id] || {}),
			id
		};
	}
}
